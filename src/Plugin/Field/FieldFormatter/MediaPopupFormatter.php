<?php

namespace Drupal\media_popup\Plugin\Field\FieldFormatter;

// File: src/Plugin/Fileld/FieldFormatter/MediaPopupFormatter.php.
use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\File\FileSystemInterface;
use Drupal\media\MediaInterface;
use Drupal\media_popup\Services\MediaPopupFileUrlGenerator;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'media_popup' formatter.
 *
 * @FieldFormatter(
 *   id = "media_popup",
 *   label = @Translation("Media Popup"),
 *   field_types = {
 *     "video",
 *     "entity_reference",
 *     "image",
 *     "audio",
 *     "file",
 *   }
 * )
 */
class MediaPopupFormatter extends FormatterBase {

  /**
   * The file URL generator service.
   *
   * @var \Drupal\media_popup\Services\MediaPopupFileUrlGenerator
   */
  protected $fileUrlGenerator;

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * Constructs a new MediaPopupFormatter object.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The field definition.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\media_popup\Services\MediaPopupFileUrlGenerator $file_url_generator
   *   The file URL generator service.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system service.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, MediaPopupFileUrlGenerator $file_url_generator, FileSystemInterface $file_system) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->fileUrlGenerator = $file_url_generator;
    $this->fileSystem = $file_system;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('media_popup.file_url_generator'),
      $container->get('file_system')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {

    $elements = [];
    foreach ($items as $delta => $item) {
      // Get the referenced media entity.
      $media_entity = $item->entity;
      if ($media_entity instanceof MediaInterface) {
        // Check if the field exists on the media entity.
        if ($media_entity->hasField('field_media_oembed_video')) {
          // Get the media video URL.
          $video_url = $media_entity->get('field_media_oembed_video')->getValue()[0]['value'];
          if (!empty($video_url)) {
            $pattern = '/(?:youtu\.be\/|youtube\.com\/(?:[^\/]+\/.+\/|(?:v|e(?:mbed)?)\/|.*[?&]v=)|youtu\.be\/)([^"&?\/\s]{11})/';
            preg_match($pattern, $video_url, $matches);
            if (isset($matches[1])) {
              $videoId = $matches[1];
              // Construct the embed URL.
              $embedUrl = 'https://www.youtube.com/embed/' . $videoId;
            }
            else {
              // Check if the video URL is a Vimeo URL.
              if (strpos($video_url, 'vimeo.com') !== FALSE) {
                // Extract the video ID from the Vimeo URL.
                $vimeoPattern = '/vimeo\.com\/(\d+)/';
                preg_match($vimeoPattern, $video_url, $vimeoMatches);
                if (isset($vimeoMatches[1])) {
                  $vimeoVideoId = $vimeoMatches[1];
                  // Construct the Vimeo embed URL.
                  $embedUrl = 'https://player.vimeo.com/video/' . $vimeoVideoId;
                }
              }
            }
            // Customize the rendering.
            $video_markup = new FormattableMarkup('<div class="mpu-popup-container"><button class="mpu-show-pop-up"> <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="           
                            position: absolute;
                            width: 32px;
                            top: 23%;
                            left: 24%;
                            color: white;">
                            <polygon points="5 3 19 12 5 21 5 3"></polygon>
                            </svg></button><iframe src="@url" frameborder="0" allowtransparency="" class="mpu-media-oembed-content" title="@title"></iframe></div>', [
                              '@url' => $embedUrl,
                              '@title' => $media_entity->label(),
                            ]);

            // Store the markup in $elements[$delta].
            $elements[$delta] = [
              '#markup' => $video_markup,
            ];
          }
        }
        elseif ($media_entity->hasField('field_media_document')) {
          // Get the media document URL.
          $file_entity = $media_entity->get('field_media_document')->entity;
          $document_url = $this->fileUrlGenerator->generateAbsoluteFileUrl($file_entity->getFileUri());

          // Create a unique ID for the iframe.
          // $unique_id = 'mpu_doc_' . uniqid();
          // Customize the rendering or store the markup in $elements[$delta].
          $document_markup = new FormattableMarkup('<div class="mpu-popup-container"><button  class ="mpu-show-pop-up mpu-pdf" type="button" data-pdf-url="@url" target="_blank">@title</button></div>', [
            '@url' => $document_url,
            '@title' => $media_entity->label(),
          ]);
          // Store the markup in $elements[$delta].
          $elements[$delta] = [
            '#markup' => $document_markup,
          ];
        }
        elseif ($media_entity->hasField('field_media_video_file')) {
          // Handle video file.
          $file_entity = $media_entity->get('field_media_video_file')->entity;
          $file_url = $this->fileUrlGenerator->generateAbsoluteFileUrl($file_entity->getFileUri());
          $video_markup = new FormattableMarkup('<div class="mpu-popup-container"><button class="mpu-show-pop-up"> <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" style="
                          position: absolute;
                          width: 32px;
                          top: 23%;
                          left: 24%;
                          color: white;">
                          <polygon points="5 3 19 12 5 21 5 3"></polygon>
                          </svg></button><video class="mpu-media-oembed-content" title="@title"><source src="@url" type="video/mp4"><source src="@url" type="video/ogg">  Your browser does not support the video tag.</video></div>', [
                            '@url' => $file_url,
                            '@title' => $media_entity->label(),
                          ]);

          // Store the markup in $elements[$delta].
          $elements[$delta] = [
            '#markup' => $video_markup,
          ];
        }
        elseif ($media_entity->hasField('field_media_image')) {
          $file_entity = $media_entity->get('field_media_image')->entity;
          $image_url = $this->fileUrlGenerator->generateAbsoluteFileUrl($file_entity->getFileUri());
          $video_markup = new FormattableMarkup('<div class="mpu-popup-container"> <img src="@url" class="mpu-image mpu-show-pop-up" title="@title"></div>', [
            '@url' => $image_url,
            '@title' => $media_entity->label(),
          ]);
          $elements[$delta] = [
            '#markup' => $video_markup,
          ];
        }
        else {
          $elements[$delta] = parent::viewElements($items, $langcode);
        }
      }
    }
    return $elements;
  }

}
