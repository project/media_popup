<?php

namespace Drupal\media_popup\Services;

use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\File\FileUrlGenerator;

/**
 * Service for generating file URLs.
 */
class MediaPopupFileUrlGenerator {

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The file URL generator service.
   *
   * @var \Drupal\Core\UrlGeneratorInterface
   */
  protected $fileUrlGenerator;

  /**
   * Constructs a new MediaPopupFileUrlGenerator object.
   *
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system service.
   * @param \Drupal\Core\UrlGeneratorInterface $file_url_generator
   *   The file URL generator service.
   */
  public function __construct(FileSystemInterface $file_system, FileUrlGenerator $file_url_generator) {
    $this->fileSystem = $file_system;
    $this->fileUrlGenerator = $file_url_generator;
  }

  /**
   * Generates an absolute file URL.
   *
   * @param string $uri
   *   The file URI.
   *
   * @return string
   *   The absolute file URL.
   */
  public function generateAbsoluteFileUrl($uri) {
    // Use $this->fileUrlGenerator directly.
    return $this->fileUrlGenerator->generateAbsoluteString($uri);
  }

}
