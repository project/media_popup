(function ($, Drupal) {
    Drupal.behaviors.customPopup = {
      attach: function (context) {
        $(document).ready(function () {
          // Check if the popup element already exists
          if ($('#mpu-popup').length === 0) {
            const yourHTMLContent = '<div id="mpu-popup" class="mpu-popup-cl"> <div class="mpu-popup-content"> <button id="mpu-close-btn" class="mpu-closs-btn-class"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" style="color: white; width: 89px; height: 39px; " fill="currentColor" class="bi bi-x" viewBox="0 0 16 16"><path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z"/></svg></button><video id="mpu-video-url" width="320" height="240" controls><source src="" type="video/mp4"><source src="" type="video/ogg"> Your browser does not support the video tag.</video> <iframe id="mpu-iframe-url" allow="fullscreen;" frameborder="0" src="" title="Europrivacy - Introductory video"></iframe><img src="" id="mpu-image-popup" title="@title"></div></div>';
            $(yourHTMLContent).prependTo('body');
          }
          $('#mpu-close-btn').on('click', function () {
            const popIframeElement = $('#mpu-iframe-url');
            if (popIframeElement.length) {
              popIframeElement.attr('src', "");
            }
            const popVideoElement = $('#mpu-video-url');
            if (popVideoElement.length) {
              popVideoElement.attr('src', "");
            }
            const popImgElement = $('#mpu-image-url');
            if (popImgElement.length) {
              popImgElement.attr('src', "");
            }
            const popup = $('#mpu-popup');
            popup.hide();
            $('body').removeClass('freeze');
          });
        });

        const popup = $(context).find('.mpu-show-pop-up');
        popup.on('click', function () {
          const container = $(this).closest('.mpu-popup-container');
          const documentUrl = $(this).data('pdf-url');
          const videoElement = container.find('video');
          const iframeElement = container.find('iframe');
          const imageElement = container.find('img');
          // Find the <iframe> and <video> elements within the container
          if (videoElement.length) {
            const videoUrl = videoElement.find('source').attr('src');
            const popVideoElement = $('#mpu-video-url');
            const popIframeHide = $('#mpu-iframe-url');
            const popImageHide = $('#mpu-image-popup');
            popVideoElement.show();
            popIframeHide.hide();
            popImageHide.hide();
            popVideoElement.attr('src', videoUrl);
          } else if (iframeElement.length) {
            const iframeUrl = iframeElement.attr('src');
            const popVideoElement = $('#mpu-iframe-url');
            const popVideoHide = $('#mpu-video-url');
            const popImageHide = $('#mpu-image-popup');
            popVideoHide.hide();
            popImageHide.hide();
            popVideoElement.show();
            popVideoElement.attr('src', iframeUrl);
          } else if (documentUrl) {
            const popVideoElement = $('#mpu-iframe-url');
            const popVideoHide = $('#mpu-video-url');
            const popImageHide = $('#mpu-image-popup');
            popVideoHide.hide();
            popImageHide.hide();
            popVideoElement.show();
            popVideoElement.attr('src', documentUrl);
          } else if (imageElement.length) {
            const imageUrl = imageElement.attr('src');
            const popVideoElement = $('#mpu-iframe-url');
            const popVideoHide = $('#mpu-video-url');
            const popimageElement = $('#mpu-image-popup');
            popVideoHide.hide();
            popVideoElement.hide();
            popimageElement.attr('src', imageUrl);
            popimageElement.show();
          }
          const popup = $('#mpu-popup');
          popup.show();
          $('body').addClass('freeze');
        });
      },
    };
  })(jQuery, Drupal);
