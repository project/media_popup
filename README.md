##CONTENTS OF THIS FILE

 - Introduction
 - Requirements
 - Installation
 - Configuration
 - Maintainers
 - Links


##INTRODUCTION

This module gives the popup field formatter for the media field such as
remote video, video, image, files


##REQUIREMENTS

No special requirements.

##INSTALLATION

 - Install as you would normally install a contributed Drupal module.

 - Or You can install it using composer,
   $ composer require drupal/media_popup

 - You can also install it using drupal console cli,
   $ drupal module:install media_popup


##CONFIGURATION
---

This module dont need Configuration just simply select media popup on
display format in media field


##MAINTAINERS

 - Ramaiya Agalcha (Ramaiya.a) - https://www.drupal.org/u/RamaiyaA


##LINKS

Project page: https://www.drupal.org/project/image_popup
